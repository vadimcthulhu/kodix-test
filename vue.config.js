module.exports = {
    css: {
        loaderOptions: {
            sass: {
                data: `
                  @import "@/styles/libs/mixins.scss";
                `
            }
        }
    }
};