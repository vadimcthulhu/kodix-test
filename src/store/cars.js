import Vue from 'vue';
import axios from 'axios';

export default {
    namespaced: true,
    state: {
        newId: 0,
        list: {}
    },
    getters: {
        list: state => {
            return state.list;
        }
    },
    mutations: {
        setCarList(state, payload) {
            state.list = payload.list;
            state.newId = state.list.length + 1;
        },
        deleteCar(state, payload) {
            const index = state.list.map(car => car.id).indexOf(payload.id);
            Vue.delete(state.list, index);
        },
        addCar(state, payload) {
            Vue.set(state.list, state.list.length, {...payload.item, id: state.newId});
            state.newId++;
        }
    },
    actions: {
        carsInit({commit}) {
            return axios({
                method: 'GET',
                url: 'https://rawgit.com/Varinetz/e6cbadec972e76a340c41a65fcc2a6b3/raw/90191826a3bac2ff0761040ed1d95c59f14eaf26/frontend_test_table.json',
            }).then((response) => {
                commit({
                    type: 'setCarList',
                    list: response.data
                });
            });
        },
    },
}