import Vue from 'vue'
import Vuex from 'vuex'
import cars from '@/store/cars'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: {
        cars
    }
})
